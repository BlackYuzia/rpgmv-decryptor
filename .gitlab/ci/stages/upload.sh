#!/bin/bash

# Load Functions
source .gitlab/ci/functions/upload.sh

# Load Variables
source .gitlab/ci/variables/base.sh
source .gitlab/ci/variables/upload.sh

# Job Commands
for binary_name in "${binaries_array[@]}"; do
    upload_file "$binary_folder" "$binary_name"
done
