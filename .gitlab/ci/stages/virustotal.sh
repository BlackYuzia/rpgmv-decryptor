#!/bin/bash
# Load Functions
source .gitlab/ci/functions/virustotal.sh

# Load Variables
source .gitlab/ci/variables/base.sh
source .gitlab/ci/variables/virustotal.sh

# Init variables
declare -xr virustotal_upload_url="$(get_upload_url "$virustotal_api_url")"
declare -xa md5_array=()
declare -xa id_array=()

# Parse files id and md5
for i in "${!binaries_array[@]}"; do
    id_array+=("$(get_file_id "$virustotal_upload_url" "./${binary_folder}/${binaries_array[$i]}")")
    md5_array+=($(get_file_md5 "$virustotal_api_url" "${id_array[$i]}"))
done

# Storage
echo "declare -xar md5_array=(${md5_array[@]})" >.gitlab/ci/storage/virustotal.storage # store
