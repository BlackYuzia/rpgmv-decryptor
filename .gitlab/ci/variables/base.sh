#!/bin/bash

declare -xr binary_folder="bin"
declare -xr binary_file="RPG-MV-MZ-Decrypter-${CI_COMMIT_TAG}"
declare -xr binary_path="${binary_folder}/${binary_file}"
declare -xr binaries_array=(
    "${binary_file}-win.exe"
    "${binary_file}-linux"
    "${binary_file}-macos"
    
)
declare -xr binaries_names=(
    "Windows"
    "Linux"
    "MacOS"
)
