# **Changelog**

All changes would be writed here.

## **[1.3.5] - 04.01.2024**

Through messages, I was informed that the photo processing mode without a key may be compromised in certain games. To prevent potential unpleasant experiences for users of the application, the option `Decrypt Images (w/out key)` has been **disabled**. 
In order to maintain user experience, option `Decrypt Images Only (with key)` has been added, which is essentially a preset for the `Decrypt Files (with key)` option with an image filter. I am not aware of the specific games in which this functison may or will be compromised, so I think this is the best way to avoid that.

> Short description - in 99% case that nothing change, just use as usual first or second mode (decrypt all or images). That's all. If something wrong - report it to me and I will try fix your issue :3

Hello ~~world~~ first fix in 2024 :eyes:

## **[1.3.4] - 24.12.2023**

- Improved prompt message if the app doesn't find any encrypted files.
> We have found that the current System.json file does not contain any encrypted data, such as images or audio. This means that you can interact with them without decryption. However, if you are certain that this is an error, you may proceed.
- Prevented app crashes when there is no encryption key in system.json.
- Prevented the display of subsequent messages after responding negatively to the first prompt during the update.

## **[1.3.3] - 11.10.2023**

### **Fixes**

- set update timeout to 10 seconds

> As of now, this is the most recent update, and version **1.3.3** should be **stable**. 
> There are no further features or fixes I plan to implement. 
> If you have any feature requests, please go ahead and create an issue.

## **[1.3.2] - 09.10.2023**

### **Features**

- Added option for fast download latest app in current folder

### **Fixes**

- Added os platform to telemetry data

## **[1.3.1] - 09.10.2023**

### **Fixes**

- Fix mistake with header verify mode, when using this mode app do nothing
- Fix a few more bugs

## **[1.3.0] - 08.10.2023**

### **Features**

- Added the capability to encrypt data from the `decrypted` folder into the `encrypted` folder.
  - The `decrypted` folder should contain files (`png` / `ogg` / `m4a`) for this operation, and there should be a `system.json` file located nearby (typically in `game/www/data`).
  - Supported `rpgmv` & `rpgmz` RPG Maker engines.

## **[1.2.2] - 07.10.2023**

### **Fixes**

- fix behavor for CTRL+C
  - where after pressing CTRL+C, the application would instead initiate all processes.

## **[1.2.1] - 07.10.2023**

### **Telemetry**

- Additionally, data regarding the number of files by type (`png`, `ogg`, and `m4a`) is also provided.

## **[1.2.0] - 07.10.2023**

### **Features**

- Added a feature for automatically opening a web page with the latest version of the application.
  - **Not tested on Linux and MacOS**. If the link does not open in your browser, please let us know.

## **[1.1.1] - 07.10.2023**

### **Features**

- Added display of the time spent on all processes (file search, verification, decryption, and saving, etc.).

### **Fixes**

- Fixed an error with the incorrect file verification type (`ByHeader` => `ByExtension` and vice versa) instead of the correct one.
- ~~Added new bugs~~

## **[1.1.0] - 06.10.2023**

### **Features**

- Added the ability to select file extensions.
- Added the option to choose the file verification method (to avoid incorrect files with seemingly correct file extensions).
  - Because the file verification method relies on reading the header bytes of the file, it heavily depends on the speed of the SSD/HDD disk. Therefore, the decision was made to keep both the old and new file type verification methods. By default (and as a recommendation), the old "by file extension" method will be used. This won't significantly impact the decryption results but greatly affects the application's performance speed.
- Added the display of the **total size of files** (in **MB**) in the results.



## **[1.0.0] - 06.10.2023**

### **Features**

- The application has been rewritten in TypeScript, which will facilitate its further development and maintenance.
- The application now uses significantly less memory for processing all files.
- Added the ability to select the system.json file if more than one is found.
- The application performs tasks faster compared to previous versions. However, this has not been tested yet.
- Added a check when reading system.json for the presence of encrypted files.
- The copying of unencrypted files feature is temporarily disabled.

### **Future Plans**

Planned features include:

- ~~Encryption (currently brainstorming the best implementation approach).~~ done
- The ability to select the depth of search.
- ~~The ability to copy unencrypted files.~~ done
- ~~The ability to choose file types (.rpgmvo, .rpgmvp, etc.).~~ done
 
## **[0.8.9] - 30.09.2023**

- Small fixes
- Add memory to telemetry

## **[0.8.8] - 27.03.2023**

### **Features**

- Allow users to copy **not** encrypted files to decrypted folder.

## **[0.8.7] - 22.02.2021**

### **Bug Fix**

- Fix bug with parsing json file (`system.json`). 
  - This file have a few symbols (on start and end of json file) and that could crash application. Currently should be fixed.

## **[0.8.6] - 04.11.2021**

### **Features**

- Now all modes (decrypting only images or all files) allow start from one binary file.

### **Fixes**

- Small optimisations of code.
- Add a bit more telemetry data.

## **[0.7.6] - 04.09.2021**

### **Fixes**

- add version type to sending telemety (now version of app would be sended with `png` or `all` postfix)

## **[0.7.5] - 04.09.2021**

### **Features**

- support `.ogg` - `.ogg_` and `.rpgmvo` files.
- support `.m4a` - `.m4a_` and `.rpgmvm` files.

### **Fixed**

- fix bug with sending telemetry 

## **[0.6.4] - 03.09.2021**

### **Added**

- add `app version` for telemetry (just for know what version was used by users);
- add `count of decrypted files` for telemetry (only count, **do not getting names or data of files**);
- add `scan`, `decrypt` and `save` spend time for telemetry (just for know spend time for each stage);

## **[0.6.3] - 02.09.2021**

### **Changes**

- fix bug of sending telemetry for darwin (macos). 

## **[0.6.2] - 02.09.2021**

### **Changes**

- now for sending telemetry would be used `https` protocol instead of `http`.

## **[0.6.1] - 10.08.2021**

### **Added**

- add telemetry

> Do not afraid, application collect only platform (linux, windows etc) and version (windows 7, 10 etc) of current OS, **app do not collect any else data like passwords or etc**.

## **[0.5.1] - 10.08.2021**

### **Fixed**

- fixed bug with progress bar

## **[0.5.0] - 10.08.2021**

### **Added**

- add app status titles:
  - `[Scanning files...]` - application **scan** current folder and subfolders
  - `[Decrypting files...]` - application **decrypt files** (that is very fast)
  - `[Saving files...]` - application **save files** in `decrypted/` folder (relative from disk speed I/O)
  - `[Complete]`  - application **complete** work and will close after 60 seconds.

> Do not forget check [**new examples**](/../../wikis/docs/Examples).

## **[0.4.0] - 07.08.2021**

> Application was renamed (from `DecryptAllPng` to `PNG_Decrypter`) and rewrited.

### **Added**

- add new **PNG** file types for check (`.png_`), now - `.png_` files would be decrypted too. 
- add **progress bar** with count of `decrypted` and `total` files, `spend time` etc. 
  - progress bar have two status
    - scanning
    - decrypting

### **Improved**

- improve speed optimisation for save and read files `[beta]`.
- improve disk **i/o** optimisation for saving files `[beta]`. 

### **Removed**

- remove support for **drag&drop** 
- remove support for **multi workspaces**

> Note: you also can use previus version(s) if you want use **drag&drop** or/and **multi workspaces** functions. Also, if many users request this feature in new versions I will add it in future, maybe.

## **[0.3.2] - 28-01-2021**

### **Added**

- support for drag&droped **folders** (just drag and drop any folder(s) on the binary file)
- support for drag&droped **files** (application would check current folder of file, be aware!)
- new screenshot for 0.3.2 version of app in [**examples**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/wikis/Examples)

## **[0.2.2] - 26-01-2021**

### **Added**

- a bit more debug
- new screenshot for 0.2.2 version of app in [**examples**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/wikis/Examples)

### **Changes**

- a bit-tiny-small code review

## **[0.2.1] - 10-01-2021**

Note: with this fixes, if you find any "trouble file" you can say what file would be failed.

### **Added**

- add file name what decripting in to counter of decrypted files (x of total) for better debuging

### **Changes**

- small code review
- change a few phrases

## **[0.2.0] - 02-01-2021**

### **Added**

- macos binaries support (`-macos`)
- macos binaries virustotal check

### **Changed**

- download [**releases**](https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/releases) now do not require auth with gitlab account

## **[0.1.4] - 01-01-2021**

### **Added**

- virustotal binaries check
  - win os binary check
  - lin os binary check

## **[0.1.2] - 27-12-2020**

### **Changed**

- optimisations in file parser
- small beauty in code style 

## **[0.1.1] - 27-12-2020**

### **Changed**

- fix bug with parsing file extension
- new method for parsing file extension

## **[0.0.1] - 19-12-2020**

### **Added**

- support new files extensions [`.rpgmvp`]
