import { RPGMVZ_Status } from "../enums/rpgmvz";

export interface ISystem {
    hasEncryptedImages: boolean;
    hasEncryptedAudio: boolean;
    encryptionKey: string;
    versionId: number;
    locale: string;
    gameTitle: string;
}

export interface IFile {
    path: string;
    dir: string;
    name: string;
    size: BigInt;
    status: RPGMVZ_Status;
    ext?: string; // extension after decryption (if decrypted)
    prevExt: string;
    header?: Buffer;
    body?: Buffer;
    valid?: boolean; // has valid while verified headers of decrypted files
}