import { EventEmitter } from "stream";
import { IFile, ISystem } from "../interfaces/decrypt";
import { getProgressBar } from "./progress";
import { Bar } from "cli-progress";
import { ITelemetryData } from "../interfaces/telemetry";
import { version as appVersion } from "../../package.json"
import { getTelemetryEvent, getSystemUsage, sendTelemetry, setStartTime, setEndTime } from "./telemetry";
import { StartMode, VerifyMode } from "../enums/start";
import { RPGMVZ_Decrypted } from "../enums/rpgmvz";
import { arch, freemem, platform, totalmem, type, version as osVersion } from "os";

declare interface GlobalEvents {
    // on
    // files
    on(event: "files.start", listener: (data: { name: string, total: number, stage: string }) => void): this;
    on(event: "files.new", listener: (data: any) => void): this;
    on(event: "files.complete", listener: (data: { name: string, total: number, stage: string }) => void): this;
    // decrypt
    on(event: "decrypt.start", listener: (files: number) => void): this;
    on(event: "decrypt.new", listener: (file: IFile) => void): this;
    on(event: "decrypt.complete", listener: (files: number) => void): this;
    // any
    on(event: string, listener: (...args: any) => void): this;

    // todo: add & update all events there
    // once
    // files
    // once(event: "files.new", listener: (...args: any) => void): this;
    // any
    once(event: string, listener: (...args: any) => void): this;

    // emit
    // files
    emit(event: "files.start", ...args)
    emit(event: "files.new", ...args)
    emit(event: "files.complete", ...args)
    // decrypt
    emit(event: "decrypt.start", ...args)
    emit(event: "decrypt.new", ...args)
    emit(event: "decrypt.complete", ...args)
    // any
    emit(event: string, ...args)
}

class GlobalEvents extends EventEmitter { };

export const gEvents = new GlobalEvents();

export function attachFilesEvents() {
    let files = 0;
    let bar: Bar;
    gEvents.on("files.start", ({ name, total }) => {
        bar = getProgressBar();
        bar.start(total, 0, { action: name })
    })

    gEvents.on("files.new", (search: boolean) => {
        // ? Only in search stage we set total instead of value
        if (search) {
            bar.setTotal(++files);
            return;
        }
        bar.update(++files)
    })

    gEvents.on("files.complete", ({ name, total }) => {
        bar.update(total, { action: name })
        bar.stop();
        files = 0;
    })
}
export function attachTelemetryEvents() {
    const telemetryData: ITelemetryData = {
        app: {
            version: appVersion,
            startDate: Date.now()
        },
        events: {},
        game: {
            gameTitle: "",
            hasAudio: false,
            hasImages: false,
            key: "",
            locale: "",
            versionId: 0
        },
        system: {
            platform: platform(),
            arch: arch(),
            freemem: (freemem() / 1e9).toFixed(2), // GB
            totalmem: (totalmem() / 1e9).toFixed(2), // GB
            type: type(),
            version: osVersion(),
            cwd: process.cwd()
        }

    };
    // Update start date on first start event
    gEvents.once("files.start", () => {
        telemetryData.app.startDate = Date.now();
    })

    gEvents.on("files.start", ({ name, stage }) => {
        const event = getTelemetryEvent(stage, telemetryData);
        event.startDate = performance.now();
        event.data.push({
            name: name,
            ...getSystemUsage(),
        })
    })

    // gEvents.on("files.new", (search: boolean) => { })

    gEvents.on("files.complete", ({ name, stage }) => {
        const event = getTelemetryEvent(stage, telemetryData);
        event.endDate = performance.now();
        event.data.push({
            name: name,
            ...getSystemUsage()
        } as any)
    })

    gEvents.on("system.parse", (system: ISystem) => {
        telemetryData.game.gameTitle = system.gameTitle || "no-title"
        telemetryData.game.key = system.encryptionKey
        telemetryData.game.hasAudio = system.hasEncryptedAudio
        telemetryData.game.hasImages = system.hasEncryptedImages
        telemetryData.game.locale = system.locale
        telemetryData.game.versionId = system.versionId
    })

    // todo: replace this by events: "prompts.start" & "prompts.end" with "events" inside
    gEvents.on("prompts.update", (update: boolean) => {
        const stage = "prompt.update"
        const event = getTelemetryEvent(stage, telemetryData);
        // looks weird... should I spend some time & make something better? nah, not now
        // todo:  make better
        event.data.push({
            update: update
        })
    })

    gEvents.on("prompts.startup", (startup: StartMode) => {
        const stage = "prompt.startup"
        const event = getTelemetryEvent(stage, telemetryData);
        event.data.push({
            startup
        })
    })

    gEvents.on("app.result", async (files: IFile[]) => {
        const stage = "result"
        const event = getTelemetryEvent(stage, telemetryData);
        event.endDate = performance.now();
        event.data.push({
            size: (files.reduce((acc: any, cur) => acc + cur.size, 0n) / 1000000n).toString(), // MB
            total: files.length,
            png: files.reduce((acc, cur) => (cur.ext as RPGMVZ_Decrypted === RPGMVZ_Decrypted.PNG) ? acc + 1 : acc, 0),
            ogg: files.reduce((acc, cur) => (cur.ext as RPGMVZ_Decrypted === RPGMVZ_Decrypted.OGG) ? acc + 1 : acc, 0),
            // not exist in rpgmv/rpgmz?
            m4a: files.reduce((acc, cur) => (cur.ext as RPGMVZ_Decrypted === RPGMVZ_Decrypted.M4A) ? acc + 1 : acc, 0),
        } as never)

        await sendTelemetry(telemetryData)
    })
}

export function attachTimeSpendEvents() {
    gEvents.once("files.start", () => {
        setStartTime()
    })
    gEvents.once("result", () => {
        setEndTime()
    })
}