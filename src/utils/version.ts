import { IGitlabRelease } from "../interfaces/gitlab";
import { version as appVersion } from "../../package.json"
import prompts from 'prompts';
import { ofetch } from "ofetch";
import { gEvents } from "./events";
import { open } from "out-url";
import { platform } from "node:os";
import { writeFileSync } from "node:fs";
import { exit } from "./exit";

export async function fetchLatestRelease(id = "23151803") {
    try {
        const data = await ofetch<IGitlabRelease>(`https://gitlab.com/api/v4/projects/${id}/releases/permalink/latest`, {
            ignoreResponseError: true,
            timeout: 10_000
        })

        return data
    } catch (error) {
        return null as never;
    }
}

/**
 * Custom names for os platform
 *
 * @return {*} 
 */
function getPlatform() {
    const os = platform();

    if (os === "win32") {
        return "win"
    }

    if (os === "darwin") {
        return "macos"
    }

    return os;
}

export async function downloadLatestVersionApp() {
    const release = await fetchLatestRelease()
    const versions = release.assets.links.filter(v => v.link_type === "package") // macos, linux and windows
    const latest = versions.find(v => v.name.includes(getPlatform()));

    if (!latest) {
        throw new Error("No valid app version for " + getPlatform())
    }

    const blob = await ofetch(latest?.url as string, {
        responseType: "blob"
    })

    writeFileSync(latest.name, Buffer.from(await blob.arrayBuffer()))

    return latest.name
}

export let latest: string;

/**
 * 
 *
 * @export
 * @return {*} true if version isn't latest
 */
export async function checkUpdate() {
    latest = (await fetchLatestRelease())?.name;
    return !!latest && appVersion !== latest
}

export async function promptOnUpdate() {
    if (!await checkUpdate()) {
        return; // no update
    }
    // todo: move prompt to menu.ts
    // We find new version and notify user about it
    const { update, download } = await prompts([
        {
            type: 'select',
            name: 'update',
            message: `We found  what you use app with ${appVersion} version, but we found update: ${latest}. You want update?`,
            choices: [
                { title: 'Yes', description: 'I will open gitlab.com/BlackYuzia/rpgmv-decryptor', value: true },
                { title: 'No', description: "Are you sure? Update could have new features, fixes etc", value: false },
            ],
            initial: 0
        },
        {
            type: (prev) => prev ? 'select' : null,
            name: 'download',
            message: `Choose how you want update tool?`,
            choices: [
                { title: 'Auto', description: "I will download the latest version of app for current OS", value: true },
                { title: 'Manually', description: 'I will just open gitlab.com/BlackYuzia/rpgmv-decryptor', value: false },
            ],
            initial: 0
        },
    ])

    gEvents.emit("prompts.update", [update, download])
    if (update) {
        if (download) {
            try {
                await downloadLatestVersionApp()
                exit("Please, open new version app", 0, 5)
                return; // return only on success
            } catch (error) {
                console.error("Something wrong on auto update", error)
            }
        }

        const link = "https://gitlab.com/BlackYuzia/rpgmv-decryptor/-/releases"
        try {
            await open(link);
        } catch (error) {
            console.log("Something happen wrong. Please, open link manually:", link)
        }
    }
}