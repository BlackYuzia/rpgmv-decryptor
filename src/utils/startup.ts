import { RPGMVZ_System } from "../enums/rpgmvz";
import { decryptWithKey, decryptWithoutKey, parseSystemJSON, verifyFiles } from "./decrypt";
import { verifyFiles as verifyFilesForEncrypt } from "./encrypt";
import { encryptWithKey } from "./encrypt";
import { attachFilesEvents, gEvents } from "./events";
import { exit } from "./exit";
import { scanRPGMVZFiles, scanRawFiles, scanSystemFiles } from "./files";
import { promptContinueDecryptiong, promptPreEncrypt, promptSystemSelect } from "./menu";
import { displayResultPanel } from "./messages";
import { createDecryptStorage, createEncryptStorage } from "./storage";

export async function startDecryptingAllFiles({
    extensions, verifyMode
}) {
    const systemFiles = await scanSystemFiles()

    if (!systemFiles.length) {
        gEvents.emit("app.result", [])
        exit(`I can't find ${RPGMVZ_System.System} file!\nPlease, place application to the folder with game.exe`)
        return;
    }

    let path: string;

    // If we found more like one System.json ask user which one we should to use.
    if (systemFiles.length > 1) {
        const { path: _path } = await promptSystemSelect(systemFiles)
        path = _path;
    }
    else {
        path = systemFiles[0].path
    }

    const system = parseSystemJSON(path);

    // todo: add app.error event for telemetry sending
    if (!system) {
        gEvents.emit("app.result", [])
        exit("Can't parse/open/find System.json! Report about it, if this is a bug.")
        return;
    }

    if (!system.hasEncryptedAudio && !system.hasEncryptedImages) {
        const { continued } = await promptContinueDecryptiong()
        if (!continued) {
            gEvents.emit("app.result", [])
            exit("")
            return;
        }
    }

    if (!system.encryptionKey) {
        gEvents.emit("app.result", [])
        exit("We couldn't find the encryption key in system.json.\nThis means that the game doesn't have any encrypted files.\nIf this is not the case, please inform the developer (gitlab.com/BlackYuzia).")
        return;
    }

    // Events
    attachFilesEvents();

    const files = await scanRPGMVZFiles({
        files: extensions
    });

    await verifyFiles(files, verifyMode);

    createDecryptStorage();
    await decryptWithKey(system.encryptionKey, files);
    displayResultPanel(files, system);
    gEvents.emit("app.result", files)
    exit("")
}

export async function startDecryptingImagesOnly({ extensions, verifyMode }) {
    const systemFiles = await scanSystemFiles()

    // Events
    attachFilesEvents();

    const files = await scanRPGMVZFiles({
        files: extensions
    });
    let path: string;

    // If we found more like one System.json ask user which one we should to use.
    if (systemFiles.length > 1) {
        const { path: _path } = await promptSystemSelect(systemFiles)
        path = _path;
    }
    else {
        path = systemFiles[0]?.path
    }

    const system = path ? parseSystemJSON(path) : undefined;

    if (system && (!system.hasEncryptedAudio && !system.hasEncryptedImages)) {
        const { continued } = await promptContinueDecryptiong()
        if (!continued) {
            gEvents.emit("app.result", [])
            exit("")
            return;
        }
    }

    await verifyFiles(files, verifyMode);

    createDecryptStorage();
    await decryptWithoutKey(files);
    displayResultPanel(files, system);
    gEvents.emit("app.result", files)
    exit("")
}

export async function startEncryptingAllFiles({ extensions, verifyMode }) {
    const { pre, engine } = await promptPreEncrypt();

    if (!pre) {
        gEvents.emit("app.result", [])
        exit("")
        return;
    }

    const systemFiles = await scanSystemFiles()

    if (!systemFiles.length) {
        gEvents.emit("app.result", [])
        exit(`I can't find ${RPGMVZ_System.System} file!\nPlease, place application to the folder with game.exe`)
        return;
    }

    let path: string;

    // If we found more like one System.json ask user which one we should to use.
    if (systemFiles.length > 1) {
        const { path: _path } = await promptSystemSelect(systemFiles)
        path = _path;
    }
    else {
        path = systemFiles[0].path
    }

    const system = parseSystemJSON(path);

    if (!system) {
        gEvents.emit("app.result", [])
        exit("Can't parse/open/find System.json! Report about it, if this is a bug.")
        return;
    }

    if (!system.hasEncryptedAudio && !system.hasEncryptedImages) {
        const { continued } = await promptContinueDecryptiong()
        if (!continued) {
            gEvents.emit("app.result", [])
            exit("")
            return;
        }
    }

    if (!system.encryptionKey) {
        gEvents.emit("app.result", [])
        exit("We couldn't find the encryption key in system.json.\nThis means that the game doesn't have any encrypted files and you should not encrypt them!\nIf this is not the case, please inform the developer (gitlab.com/BlackYuzia).")
        return;
    }

    // Create storages if !exist before proceeed anything
    createDecryptStorage()
    createEncryptStorage()

    // Events
    attachFilesEvents();

    const files = await scanRawFiles({ files: extensions });
    await verifyFilesForEncrypt(files, verifyMode, engine);
    await encryptWithKey(system.encryptionKey, files);
    displayResultPanel(files, system);
    gEvents.emit("app.result", files)
    exit("")
}