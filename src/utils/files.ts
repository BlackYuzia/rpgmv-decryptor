import readdirp, { ReaddirpOptions } from "readdirp"
import { gEvents } from "./events";
import { IFile } from "../interfaces/decrypt";
import { join, parse } from "path";
import { ignoreFolders } from "../enums/ignore";
import { RPGMVZ_Decrypted, RPGMVZ_Status, RPGMVZ_System, RPGMV_Encrypted, RPGMZ_Encrypted } from "../enums/rpgmvz";
import { decryptStorage } from "./storage";


export async function scan(scanPath: string, options: ReaddirpOptions) {
    const files: IFile[] = [];

    gEvents.emit("files.start", {
        name: "Searching...",
        total: 1,
        stage: "search"
    })

    for await (const entry of readdirp(scanPath, options)) {
        const { path, basename, stats } = entry;
        // todo: use path instead of basename
        const { dir, name, ext } = parse(basename);
        files.push({
            path: join(scanPath, path), // readdirp remove base path (scanPath) from path, that's wrong, so we add it manually.
            name: name,
            dir: dir,
            // ext: ext,
            prevExt: ext,
            size: stats?.size as any || 0n as never,
            status: RPGMVZ_Status.Unknown
        })

        gEvents.emit("files.new", true) // search is true
    }

    gEvents.emit("files.complete", {
        name: "Complete searching",
        total: files.length,
        stage: "search"
    })

    return files;
}

export function scanSystemFiles(path = ".") {
    return scan(path, {
        alwaysStat: false,
        depth: 6,
        directoryFilter: ignoreFolders,
        fileFilter: [
            RPGMVZ_System.System,
            RPGMVZ_System.System.toLowerCase(),
        ]
    })
}

export function scanRPGMVZFiles({
    path = ".",
    depth = 8,
    files = [
        // RPGMV
        RPGMV_Encrypted.M4A,
        RPGMV_Encrypted.PNG,
        RPGMV_Encrypted.OGG,
        // RPGMZ
        RPGMZ_Encrypted.PNG,
        RPGMZ_Encrypted.M4A,
        RPGMZ_Encrypted.OGG,
        // Not Encrypted Files
        //  RPGMVZ_Decrypted.PNG,
        //  RPGMVZ_Decrypted.M4A,
        //  RPGMVZ_Decrypted.OGG,
    ]
} = {}) {
    return scan(path, {
        alwaysStat: true,
        depth: depth,
        directoryFilter: ignoreFolders,
        fileFilter: files.map(v => `*${v}`) // transform to '*.ext' format
    });
}

export function scanRawFiles({
    path = decryptStorage,
    depth = 8,
    files = [
        RPGMVZ_Decrypted.PNG,
        RPGMVZ_Decrypted.M4A,
        RPGMVZ_Decrypted.OGG,
    ]
} = {}) {
    return scan(path, {
        alwaysStat: true,
        depth: depth,
        directoryFilter: ignoreFolders,
        fileFilter: files.map(v => `*${v}`) // transform to '*.ext' format
    });
}