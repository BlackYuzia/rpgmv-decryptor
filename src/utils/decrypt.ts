import { mkdirSync, readFileSync, writeFileSync } from "fs";
import { IFile, ISystem } from "../interfaces/decrypt";
import { open } from 'node:fs/promises';
import { RPGMVZ_Decrypted, RPGMVZ_Status, RPGMV_Encrypted, RPGMZ_Encrypted } from "../enums/rpgmvz";
import { join, parse, relative } from "path";
import { FileHandle } from "fs/promises";
import { gEvents } from "./events";
import { VerifyMode } from "../enums/start";
import { decryptStorage } from "./storage";

export const defaultHeaderLength = 16;
export const defaultSignature = "5250474d56000000";
export const defaultVersion = "000301";
export const defaultRemain = "0000000000";
export const pngHeaderBytes = Buffer.from("89504E470D0A1A0A0000000D49484452", "hex");
export const oggHeaderBytes = Buffer.from("4F6767530002000000000000000045F2", "hex"); // ! deprecated, latest 2 bytes are different from project to project :c
// ! with 99% chance this is wrong header, test it in future & I need one encrypted file (& key) for decrypt it & get valid header!
export const m4aHeaderBytes = Buffer.from("0000001c667479704d34412000000200", "hex");

export function parseSystemJSON(path: string): ISystem | undefined {
    try {
        const system: ISystem = JSON.parse(readFileSync(path).toString("utf-8").trim())
        gEvents.emit("system.parse", system)
        return system;
    } catch (e) {
        console.error(`Error on parse ${path} file!`, e)
        console.warn("If this is a bug. Report to the developer about it")
    }
}

export async function decryptWithKey(key: string, files: IFile[]) {
    const encryptBuffer = Buffer.from(key, "hex")
    gEvents.emit("files.start", {
        name: "Decrypting...",
        stage: "decrypt",
        total: files.length
    })
    for (const file of files) {
        try {
            // Messy a bit...
            if (file.status === RPGMVZ_Status.Encrypted) {
                const { dir, name } = parse(file.path);
                const path = relative(".", join(decryptStorage, dir, name + file.ext));
                const decryptedBuffer = new Uint8Array(readFileSync(file.path)).slice(16) // remove fake-header from RPGMVZ

                for (let i = 0; i < defaultHeaderLength; i++) {
                    decryptedBuffer[i] = decryptedBuffer[i] ^ encryptBuffer[i] // decrypt xor
                }

                file.header = Buffer.from(decryptedBuffer.slice(0, 16))

                mkdirSync(parse(path).dir, { recursive: true }) // create the same folders struct inside decrypted folder
                writeFileSync(path, decryptedBuffer)

                // Do this only after all manipulations with file
                file.status = RPGMVZ_Status.Decrypted
                gEvents.emit("files.new");
                continue;
            }
            if (file.status === RPGMVZ_Status.Raw) {
                const { dir, name } = parse(file.path);
                const path = relative(".", join(decryptStorage, dir, name + file.ext));

                mkdirSync(parse(path).dir, { recursive: true }) // create the same folders struct inside decrypted folder
                writeFileSync(path, readFileSync(file.path))
                continue;
            }
        } catch (error) {
            file.status = RPGMVZ_Status.Error
            console.error("Error on process with file", file, error)
        }
    }
    gEvents.emit("files.complete", {
        name: "Complete decrypting",
        stage: "decrypt",
        total: files.length
    })
}


/**
 * Work only for images (ogg has different bytes per game / key)
 *
 * @export
 * @param {IFile[]} files
 */
export async function decryptWithoutKey(files: IFile[]) {
    for (const file of files) {
        try {
            // Messy a bit...
            if (file.status === RPGMVZ_Status.Encrypted) {
                const { dir, name } = parse(file.path);
                const path = relative(".", join(decryptStorage, dir, name + file.ext));
                const bodyBuffer = new Uint8Array(readFileSync(file.path)).slice(32)
                const fileBuffer = Buffer.concat([getFileHeader(file.ext as any), bodyBuffer])
                file.header = Buffer.from(new Uint8Array(fileBuffer).slice(0, 16))

                mkdirSync(parse(path).dir, { recursive: true })
                writeFileSync(path, fileBuffer)

                // Do this only after all manipulations with file
                file.status = RPGMVZ_Status.Decrypted
                continue;
            }

            if (file.status === RPGMVZ_Status.Raw) {
                const { dir, name } = parse(file.path);
                const path = relative(".", join(decryptStorage, dir, name + file.ext));

                mkdirSync(parse(path).dir, { recursive: true }) // create the same folders struct inside decrypted folder
                writeFileSync(path, readFileSync(file.path))
                continue;
            }

        } catch (error) {
            file.status = RPGMVZ_Status.Error
            console.error("Error on process with file", file, error)
        }
    }
}

export function buildFakeHeader() {
    const fakeHeader = new Uint8Array(defaultHeaderLength);
    const structure = defaultSignature + defaultVersion + defaultRemain;

    for (let i = 0; i < defaultHeaderLength; i++) {
        fakeHeader[i] = parseInt(structure.substring(i * 2, i * 2 + 2), 16);
    }

    return fakeHeader
}


/**
 * Set header, status & ext for files
 *
 * @export
 * @param {IFile[]} files
 */
export async function verifyFiles(files: IFile[], verifyMode: VerifyMode) {
    gEvents.emit("files.start", {
        name: "Verification...",
        total: files.length,
        stage: "verify"
    })

    for (const file of files) {
        file.status = await getStatus(verifyMode, file)
        file.ext = getExtension(file.prevExt as any)
        gEvents.emit("files.new")
    }

    gEvents.emit("files.complete", {
        name: "Complete verification",
        total: files.length,
        stage: "verify",
    })
}

export function getExtension(ext: RPGMV_Encrypted | RPGMZ_Encrypted | RPGMVZ_Decrypted) {
    switch (ext) {
        case RPGMV_Encrypted.PNG:
        case RPGMZ_Encrypted.PNG:
            return ".png"
        case RPGMV_Encrypted.OGG:
        case RPGMZ_Encrypted.OGG:
            return ".ogg"
        case RPGMZ_Encrypted.M4A:
        case RPGMV_Encrypted.M4A:
            return ".m4a"
        case RPGMVZ_Decrypted.PNG:
        case RPGMVZ_Decrypted.M4A:
        case RPGMVZ_Decrypted.OGG:
            return ext;
        default:
            console.warn("Unknown extension %s, please report about it.", ext);
            return ext;
        // throw new Error("Unknown extension!", { cause: ext })
    }
}


/**
 * This used for decrypt file(s) without key
 *
 * @export
 * @param {(RPGMV_Encrypted | RPGMZ_Encrypted | RPGMVZ_Decrypted)} ext
 * @return {*} 
 */
export function getFileHeader(ext: RPGMV_Encrypted | RPGMZ_Encrypted | RPGMVZ_Decrypted): Buffer {
    switch (ext) {
        case RPGMVZ_Decrypted.PNG:
        case RPGMV_Encrypted.PNG:
        case RPGMZ_Encrypted.PNG:
            return pngHeaderBytes
        case RPGMVZ_Decrypted.OGG:
        case RPGMV_Encrypted.OGG:
        case RPGMZ_Encrypted.OGG:
            return oggHeaderBytes
        case RPGMVZ_Decrypted.M4A:
        case RPGMZ_Encrypted.M4A:
        case RPGMV_Encrypted.M4A:
            return m4aHeaderBytes
        default:
            console.warn("Unknown extension %s, please report about it.", ext);
            throw "Unknown File"
        // throw new Error("Unknown extension!", { cause: ext })
    }
}

export async function getStatus(verify: VerifyMode, file: IFile) {
    if (verify === VerifyMode.ByHeader) {
        // Only for header required
        let handle: FileHandle = null as never;
        try {
            handle = await open(file.path);
            const result = await handle.read({
                buffer: Buffer.alloc(16)
            });
            file.header = result.buffer;
        } catch (error) {
            console.error("Error on getStatus", file, error)
        }
        finally {
            await handle?.close();
        }
        return getStatusByHeader(file.header as any)
    }

    return getStatusByExtension(file.prevExt as any)
}

export function getStatusByHeader(buff: Buffer): RPGMVZ_Status {
    const RPGMVZHeader = buildFakeHeader();
    if (Buffer.compare(RPGMVZHeader, buff) === 0) {
        return RPGMVZ_Status.Encrypted
    }
    if ([
        Buffer.compare(pngHeaderBytes, buff),
        Buffer.compare(oggHeaderBytes, buff),
        Buffer.compare(m4aHeaderBytes, buff),
    ].includes(0)) {
        return RPGMVZ_Status.Raw
    }
    return RPGMVZ_Status.Unknown
}

export function getStatusByExtension(ext: RPGMV_Encrypted | RPGMZ_Encrypted | RPGMVZ_Decrypted): RPGMVZ_Status {
    switch (ext) {
        case RPGMV_Encrypted.PNG:
        case RPGMV_Encrypted.OGG:
        case RPGMV_Encrypted.M4A:
        case RPGMZ_Encrypted.PNG:
        case RPGMZ_Encrypted.OGG:
        case RPGMZ_Encrypted.M4A:
            return RPGMVZ_Status.Encrypted
        case RPGMVZ_Decrypted.PNG:
        case RPGMVZ_Decrypted.OGG:
        case RPGMVZ_Decrypted.M4A:
            return RPGMVZ_Status.Raw
        default:
            console.warn("Unknown file ext", ext)
            return RPGMVZ_Status.Unknown
    }
}

export function verifyDecryptedHeaders(files: IFile[]) {
    gEvents.emit("files.start", {
        name: "Verification...",
        total: files.length,
        stage: "verify_decrypted"
    })
    for (const file of files) {
        file.valid = [
            Buffer.compare(pngHeaderBytes, file.header as Buffer),
            Buffer.compare(oggHeaderBytes, file.header as Buffer),
            Buffer.compare(m4aHeaderBytes, file.header as Buffer),
        ].includes(0)
        gEvents.emit("files.new")
    }
    gEvents.emit("files.complete", {
        name: "Complete verification",
        total: files.length,
        stage: "verify_decrypted",
    })
}