import { Bar, Presets } from "cli-progress"

export let progressBar: Bar;

export function getProgressBar() {
    return progressBar || createProgressBar();
}

export function createProgressBar() {
    // create new container
    progressBar = new Bar({
        format: "{bar} | {action} | {value}/{total}",
    }, Presets.shades_classic)

    return progressBar;
}