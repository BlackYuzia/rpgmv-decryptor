import prompts from "prompts";
import { StartMode, VerifyMode } from "../enums/start";
import { IFile } from "../interfaces/decrypt";
import { gEvents } from "./events";
import { RPGMVZ_Decrypted, RPGMV_Encrypted, RPGMZ_Encrypted, RPGMakerEngine } from "../enums/rpgmvz";
import { decryptStorage, encryptStorage } from "./storage";


export async function promptStartup() {
    const prompt = await prompts([
        {
            type: 'select',
            name: 'startup',
            message: 'Select a startup mode',
            hint: "If you run application from folder/directory  with game exe, then you want use the first one.",
            warn: "This feature could be implemented in future releases",
            choices: [
                { title: 'Decrypt Files (with key)', description: 'This mode require System.json & encryption key inside json file for decrypt files', value: StartMode.AllFiles },
                { title: 'Decrypt Images Only (with key)', description: "Just preset for 'Decrypt Files' with images only by default, required system.json & encryption key", value: StartMode.ImagesOnly },
                // Temp disabled duo broken in some kind of games?
                { title: 'Decrypt Images (without Key)', description: "This mode usefull for decrypt images without encryption key.", disabled: true },
                { title: 'Encrypt Files', description: "This mode usefull for encrypt files after decrypting.", value: StartMode.EncryptAll },
            ],
            initial: 0,
        },
        {
            type: 'multiselect',
            name: 'extensions',
            message: 'Choose the file extensions to include in the scan.',
            choices: prev => {
                if (prev === StartMode.EncryptAll) {
                    return [
                        ...Object.values(RPGMVZ_Decrypted).map(v => {
                            return {
                                title: v,
                                value: v,
                                selected: true,
                                description: "Raw (no encrypted) file"
                            }
                        }),
                    ]
                }

                const imagesOnly = prev === StartMode.ImagesOnly;

                return [
                    ...Object.values(RPGMV_Encrypted).map(v => {
                        return {
                            title: v,
                            value: v,
                            selected: imagesOnly ? v === RPGMV_Encrypted.PNG : true,
                            description: "Encrypted file from RPGMV engine"
                        }
                    }),
                    ...Object.values(RPGMZ_Encrypted).map(v => {
                        return {
                            title: v,
                            value: v,
                            selected: imagesOnly ? v === RPGMZ_Encrypted.PNG : true,
                            description: "Encrypted file from RPGMZ engine"
                        }
                    }),
                    ...Object.values(RPGMVZ_Decrypted).map(v => {
                        return {
                            title: v,
                            value: v,
                            selected: false,
                            description: "Raw (no encrypted) file"
                        }
                    }),
                ]
            },
        },
        {
            type: 'select',
            name: 'verifyMode',
            message: 'Choose a method for file verification/validation',
            hint: "To be honest, none of these types will significantly impact your experience, except for speed. Speed is a compelling factor, so in 99% of cases, you can or should opt for the fastest option.",
            choices: [
                {
                    title: "by Extension [fast]",
                    value: VerifyMode.ByExtension,
                    description: "less reliable but fastest"
                },
                {
                    title: "by Header [slow]",
                    value: VerifyMode.ByHeader,
                    description: "more reliable but slower"
                },
            ],
            initial: 0
        }
    ])

    gEvents.emit("prompts.startup", prompt)

    return prompt;
}

export async function promptSystemSelect(files: IFile[]) {
    return await prompts([
        {
            type: 'select',
            name: 'path',
            message: 'Select which system.json file you want to use',
            hint: "If you see more like one file, pls, be sure you pick right one. Wrong system.json will cause errors on decrypt files duo wrong encryption/decryption key",
            choices: files.map(v => ({
                title: v.path,
                value: v.path
            })),
            initial: 0,
        }
    ])
}

export async function promptContinueDecryptiong() {
    return await prompts([
        {
            type: 'confirm',
            name: 'continued',
            message: 'We have found that the current System.json file does not contain any encrypted data, such as images or audio.\nThis means that you can interact with them without decryption.\nHowever, if you are certain that this is an error, you may proceed.',
            initial: true
        }
    ])
}

export async function promptPreEncrypt() {
    return await prompts([
        {
            type: "select",
            name: "engine",
            message: `Please, select RPG Maker Engine.`,
            hint: `App should know which extensions should use for save files.`,
            initial: 0,
            choices: [
                {
                    title: "RPG Maker MV",
                    value: RPGMakerEngine.MV,
                    description: Object.values(RPGMV_Encrypted).join(", ")
                },
                {
                    title: "RPG Maker MZ",
                    value: RPGMakerEngine.MZ,
                    description: Object.values(RPGMZ_Encrypted).join(", ")
                },
            ]
        },
        {
            type: "toggle",
            name: "pre",
            message: `I will start encrypting files from ${decryptStorage} folder to the ${encryptStorage}.\nThis mode require System.json with encryption key for work.\nDon't put me (app) to the ${decryptStorage} folder!\nYou should have something inside ${decryptStorage} folder before run encrypt mode.\nIf you sure what you do all right, then you can continue.`,
            initial: true,
        },
    ])
}